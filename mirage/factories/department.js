import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  name: faker.list.random('Technical', 'Accounts', 'Marketing'),
  location: faker.address.city()

});
