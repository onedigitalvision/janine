import {association, Factory, faker} from 'ember-cli-mirage';

export default Factory.extend({
  firstname: faker.list.random(faker.name.firstName(),faker.name.firstName(),faker.name.firstName(),faker.name.firstName()),
  surname: faker.list.random(faker.name.lastName(),faker.name.lastName(),faker.name.lastName(),faker.name.lastName()),
  email: faker.list.random(faker.internet.email(),faker.internet.email(),faker.internet.email(),faker.internet.email(),faker.internet.email()),
  job: faker.list.random(faker.name.jobTitle(),faker.name.jobTitle(),faker.name.jobTitle(),faker.name.jobTitle(),faker.name.jobTitle(),faker.name.jobTitle(),faker.name.jobTitle()),
  city: faker.list.random(faker.address.city(),faker.address.city(),faker.address.city(),faker.address.city()),
  image: faker.list.random(faker.internet.avatar(),faker.internet.avatar(),faker.internet.avatar(),faker.internet.avatar()),
  department: association()
});
