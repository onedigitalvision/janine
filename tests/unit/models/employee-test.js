import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupModelTest } from 'ember-mocha';
import Ember from 'ember'

describe('Unit | Model | employee', function() {
  setupModelTest('employee', {
    // Specify the other units that are required for this test.
      needs: [
        'model:department'
      ]
  });

  // Replace this with your real tests.
  it('exists', function() {
    let model = this.subject();
    // var store = this.store();
    expect(model).to.be.ok;
  });

  it('belongs to department', function () {
    let model = this.store().modelFor('employee');

    let relationship = Ember.get(model, 'relationshipsByName').get('department');

    expect(relationship.key).to.eql('department')

    expect(relationship.kind).to.eql('belongsTo')
  })
});
