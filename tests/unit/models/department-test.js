import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupModelTest } from 'ember-mocha';
import Ember from 'ember';

describe('Unit | Model | department', function() {
  setupModelTest('department', {
    // Specify the other units that are required for this test.
      needs: ['model:employee']
  });

  // Replace this with your real tests.
  it('exists', function() {
    let model = this.subject();
    // var store = this.store();
    expect(model).to.be.ok;
  });

  it('has many employees', function () {
    let model =  this.store().modelFor('department');
    let relationship = Ember.get(model, 'relationshipsByName').get('employees');

    expect(relationship.key).to.eql('employees');
    expect(relationship.kind).to.eql('hasMany')
  })
});
