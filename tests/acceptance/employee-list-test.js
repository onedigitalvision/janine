/* eslint-disable no-undef */
import {describe, it, beforeEach, afterEach} from 'mocha';
import {expect} from 'chai';
import startApp from '../helpers/start-app';
import destroyApp from '../helpers/destroy-app';
// import {startMirage} from '../helpers/setup-mirage-for-testing';

describe('Acceptance | employee list', function () {
  let application;

  beforeEach(function () {
    application = startApp();
    // startMirage(this.container);
    server.loadFixtures();

  });

  afterEach(function () {
    destroyApp(application);
  });

  it('can visit /employee-list', function () {
    visit('/employee-list');

    return andThen(() => {
      expect(currentURL()).to.equal('/employee-list');
    });
  });

  it('shows 4 employees', function () {
    server.createList('department', 3);
    server.createList('employee', 4);
    visit('/employee-list');
    return andThen(() => {
      expect(find(".employees__item").length).to.equal(4);
    });
  });
});


