import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import startApp from 'janine/tests/helpers/start-app';
import destroyApp from 'janine/tests/helpers/destroy-app';

describe('Acceptance | index', function() {
  let application;

  beforeEach(function() {
    application = startApp();
  });

  afterEach(function() {
    destroyApp(application);
  });

  it('can visit /', function() {
    visit('/');

    return andThen(() => {
      expect(currentURL()).to.equal('/employee-list');
    });
  });
});
