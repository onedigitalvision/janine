import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupComponentTest } from 'ember-mocha';
import hbs from 'htmlbars-inline-precompile';

describe('Integration | Component | employee item', function() {

  setupComponentTest('employee-item', {
    integration: true
  });

  it('renders', function() {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });
    // Template block usage:
    // this.render(hbs`
    //   {{#employee-item}}
    //     template content
    //   {{/employee-item}}

    this.render(hbs`{{employee-item}}`);
    expect(this.$()).to.have.length(1);
  });

  it('expands employee when clicked', function () {
    this.$('employees__item').click();
    expect(this.$('.employees__item').hasClass("employees__item--expanded"))
  })
});
