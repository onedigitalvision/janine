import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupComponentTest } from 'ember-mocha';
import hbs from 'htmlbars-inline-precompile';


describe('Integration | Component | employee list', function() {

  setupComponentTest('employee-list', {
    integration: true
  });


  it('renders', function() {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });
    // Template block usage:
    // this.render(hbs`
    //   {{#employee-list}}
    //     template content
    //   {{/employee-list}}
    // `);

    this.render(hbs`{{employee-list}}`);
    expect(this.$()).to.have.length(1);
  });
});
