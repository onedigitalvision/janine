Janine HR - Ember Demo App
===

![enter image description here](http://www.blastr.com/sites/blastr/files/styles/square_96x96/public/janinemelnitz.jpg?itok=hnBmDHb8)

A ficticous HR App
---
A brief guide to what I've put together
  
 - Employee Department data model
	 - Employees belongTo a department
	 - A department hasMany employees
 - Basic Ember routing
 - Nested routing
	 - `employee-list/index`
	 - `employee-list/edit`
 - TDD implementation 
	 - Familiarising myself with Mocha/Chai as a testing framework
 - BEM and Stylus used 

I've a rails based JSON API which I can integrated if required at the minute the app is using Ember mirage to generate data which will not persist on refresh

**Thought's on ember**

I like it, its very similar to rails unsurprisingly as it was created by a former rails core developer. It's a lot more structured than angular and lots easy for TDD 

**Todo**

I want to look into Services, Custom Helpers and more complex components,. 

**Questions** 

 1. How do you use controllers as they seem to have very little involvement in ember based on the docs and what I've read online?
 2. Whats best practice for trigger CRUD actions, i've put my CRUD action inside my components as they seem to deprecating controllers?
 
----------

Ember Notes and understandings
---

Data
---

It looks ember is handling the marrying up of the mirage data store for me probably by registering the adapter connecting based on namespace.

**Defining relationships**

It's very, very similar to rails in the respect 
* hasMany
* belongsTo

Much like rails you need to be mindful of singular and plural names 

Components
---
Ok so I had built a `employee-list` component and my desired behaviour was when  you  click on an employee the employee expands … I created a method in the employee-list component to handle this behaviour 
```
  isExpanded: false,
  actions: {
    expandEmployee() {
      this.toggleProperty('isExpanded');
    }
  }
```
it worked to a degree however all of the employee’s expanded having consider why that may happen it became obvious that components actually need to be more granular so I then created `employee-item` which actually makes a lot sense to be it is scopes, the scope of an `employee-list` affects all items and the scope of an `employee-item` affects the singular item

Styles
---
I've never used Stylus but hey lets give it go its not that different from SASS also read in the job spec you guys adhere to BEM for your styles giving that a go aswell
  

Testing
---
Very familiar to looking though it, when I was for [Initforthe](http://initforthe.com) we we're primarily a rails house and there was emphasis testing using [Rspec](http://rspec.info/) and [Capybara](http://teamcapybara.github.io/capybara/) 

Debugging
---
I wanted to using break points for debugging my tests turns out putting `debugger;` in my code makes chrome devtools kick in :)

Bugs
---
When testing whether all items are shown with employee list we're getting double the items i.e. our test looks like this
```
  it('shows 4 employees', function () {
    server.createList('department', 3);
    server.createList('employee', 4);
    visit('/employee-list');
    return andThen(() => {
      expect(find(".employees__item").length).to.equal(4);
    });
  });
```
however the `find('.employee__item')` produces a result of 8 coming back to thisfir
