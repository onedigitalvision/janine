import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('employee-list', function() {
    this.route('edit', {path: '/:employee_id'});
  });
  this.route('department');
});

export default Router;
