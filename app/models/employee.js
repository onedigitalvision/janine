import DS from 'ember-data';

export default DS.Model.extend({
  firstname: DS.attr(),
  surname: DS.attr(),
  email: DS.attr(),
  job: DS.attr(),
  city: DS.attr(),
  image: DS.attr(),
  department: DS.belongsTo('department', {inverse: 'employees'})
});
