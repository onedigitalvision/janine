import Ember from 'ember';

export default Ember.Component.extend({
  isExpanded: false,
  actions: {
    expandEmployee() {
      this.toggleProperty('isExpanded');
    }
  }
});
